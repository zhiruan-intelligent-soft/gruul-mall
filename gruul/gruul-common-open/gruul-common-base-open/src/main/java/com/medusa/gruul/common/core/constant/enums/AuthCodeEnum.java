package com.medusa.gruul.common.core.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 短信验证码
 *
 * @author whh
 */
@Getter
@AllArgsConstructor
public enum AuthCodeEnum {

    /* 平台模块1000-2000  start================>*/
    MINI_LOGIN(1001, "短信登录校验"),
    ACCOUNT_PHONE_IN_TIE(1003, "用户手机号换绑校验"),
    ACCOUNT_PASSWORD_IN_TIE(1004, "用户修改密码"),
    ACCOUNT_FORGET_PASSWD(1007, "用户忘记密码找回"),



    /* 平台模块1000-2000   end================>*/

    /**
     * 认证失败
     */
    AUTH_FAIL(3, "认证失败"),

    /**
     * 过期jwt
     */
    AUTH_EXPIRES(4, "认证过期"),
    ;
    /**
     * 类型
     */
    private int type;

    /**
     * 描述
     */
    private String description;

    public static boolean isExistValue(Integer type) {
        for (AuthCodeEnum value : AuthCodeEnum.values()) {
            if (value.getType() == type) {
                return true;
            }
        }
        return false;
    }


}
