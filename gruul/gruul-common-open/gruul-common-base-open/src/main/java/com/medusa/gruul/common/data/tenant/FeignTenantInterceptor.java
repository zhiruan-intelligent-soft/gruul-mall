

package com.medusa.gruul.common.data.tenant;

import com.medusa.gruul.common.core.constant.CommonConstants;
import com.medusa.gruul.common.core.util.CurUserUtil;
import com.medusa.gruul.common.dto.CurUserDto;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @Description: FeignTenantInterceptor.java
 * @Author: alan
 * @Date: 2019/7/13 19:32
 */
@Slf4j
public class FeignTenantInterceptor implements RequestInterceptor {
	/**
	 * 请求拦截器，用于在发送请求前设置请求头
	 * @param requestTemplate 请求模板，包含请求的相关信息
	 */
	@Override
	public void apply(RequestTemplate requestTemplate) {
		CurUserDto dto = CurUserUtil.getHttpCurUser();
		if (dto == null) {
			return;
		}
		if (StringUtils.isBlank(dto.getUserId())) {
			log.error("TTL 中的 用户为空，feign拦截器 >> 增强失败");
			return;
		}
		// 设置请求头，用于传递用户ID
		requestTemplate.header(CommonConstants.CUR_USER, CurUserUtil.getHttpCurUser().getUserId());
	}

}
