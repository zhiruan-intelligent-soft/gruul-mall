package com.medusa.gruul.common.data.tenant;

import com.medusa.gruul.common.core.constant.CommonConstants;
import com.medusa.gruul.common.redis.RedisManager;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Slf4j
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TenantContextHolderFilter extends GenericFilterBean {
    /**
     * 过滤器类，用于在处理 HTTP 请求时进行用户认证和授权
     * <p>
     * 这个过滤器的主要目的是检查请求头中的令牌（token）。如果令牌存在且有效，它会从 Redis 缓存中获取与令牌关联的用户信息
     * ，并将该用户信息设置到当前用户上下文中。这样，后续的处理逻辑就可以根据当前用户的上下文信息进行认证和授权决策
     * <p>
     * 如果请求头中不存在令牌或者令牌无效，过滤器不会执行任何操作，而是直接将请求传递给下一个过滤器或资源
     * <p>
     * 在过滤器链中的正确位置配置这个过滤器，确保它可以在适当的时候拦截和处理请求
     *
     * @author [作者姓名]
     * @since [版本号]
     */
    @Override
    @SneakyThrows
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String token = request.getHeader(CommonConstants.TOKEN);
        if (StringUtils.isNotBlank(token)) {
            RedisManager redisManager = RedisManager.getInstance();
            String user = redisManager.get(StringUtils.isBlank(token)? CommonConstants.CUR_USER : token);
            CurUserContextHolder.setCurUser(user);
            log.debug("获取header中的token为:{}", token);
        }

        filterChain.doFilter(request, response);

    }

}
