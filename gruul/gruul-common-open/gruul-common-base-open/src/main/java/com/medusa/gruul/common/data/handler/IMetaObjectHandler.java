package com.medusa.gruul.common.data.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Description: 填充器
 * @Author: alan
 * @Date: 2019/8/31 9:37
 */
@Slf4j
@Component
public class IMetaObjectHandler implements MetaObjectHandler {

	@Override
	/**
	 * 在插入数据库记录之前，填充默认字段值。
	 *
	 * 该方法设置了三个字段的值：
	 * - deleted：设置为 false，表示该记录未被删除。
	 * - createTime：设置为当前的本地日期时间，表示记录的创建时间。
	 * - updateTime：设置为当前的本地日期时间，表示记录的更新时间。
	 *
	 * @param metaObject 包含数据库字段和对应值的元对象
	 */
	public void insertFill(MetaObject metaObject) {
		log.info("start insert fill....");
		this.setInsertFieldValByName("deleted", false, metaObject);
		this.setInsertFieldValByName("createTime", LocalDateTime.now(), metaObject);
		this.setInsertFieldValByName("updateTime", LocalDateTime.now(), metaObject);
	}

	@Override
	/**
	 * 在更新数据库记录之前，填充默认字段值
	 *
	 * 该方法设置了一个字段的值：
	 * - updateTime：设置为当前的本地日期时间，表示记录的更新时间
	 *
	 * @param metaObject 包含数据库字段和对应值的元对象
	 */
	public void updateFill(MetaObject metaObject) {
		log.info("start update  fill....");
		this.setUpdateFieldValByName("updateTime", LocalDateTime.now(), metaObject);

	}


}
