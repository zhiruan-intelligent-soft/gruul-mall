package com.medusa.gruul.goods.mapper.api;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.medusa.gruul.goods.api.entity.Product;
import org.springframework.stereotype.Repository;

/**
 * 小程序商品信息 Mapper接口
 *
 * @author lcy
 * @since 2020-08-17
 */
@Repository
public interface ApiProductSkuMapper extends BaseMapper<Product> {


}
