package com.medusa.gruul.goods.mapper.api;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.medusa.gruul.goods.api.entity.SaleMode;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 商品自定义专区 Mapper 接口
 * </p>
 *
 * @author alan
 * @since 2019-09-03
 */
@Repository
public interface ApiSaleModeMapper extends BaseMapper<SaleMode> {

}
