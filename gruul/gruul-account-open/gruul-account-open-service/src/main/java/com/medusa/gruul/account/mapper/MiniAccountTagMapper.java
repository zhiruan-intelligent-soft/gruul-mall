package com.medusa.gruul.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.medusa.gruul.account.api.entity.MiniAccountTag;

/**
 * <p>
 * 用户标签表 Mapper 接口
 * </p>
 *
 * @author whh
 * @since 2019-11-18
 */
public interface MiniAccountTagMapper extends BaseMapper<MiniAccountTag> {


}
