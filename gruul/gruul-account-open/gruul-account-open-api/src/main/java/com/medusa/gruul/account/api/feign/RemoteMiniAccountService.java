package com.medusa.gruul.account.api.feign;

import com.medusa.gruul.account.api.model.AccountInfoDto;
import com.medusa.gruul.account.api.model.MiniAccountExtDto;
import com.medusa.gruul.account.api.model.MiniAccountExtendsUpdateDto;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author whh
 * @date 2019/11/06
 */
@FeignClient(value = "account-open")
public interface RemoteMiniAccountService {


    /**
     * 修改用户扩展字段部分数据
     *
     * @param userId                      用户id
     * @param miniAccountExtendsUpdateDto com.medusa.gruul.account.api.model.MiniAccountExtendsUpdateDto
     * @return java.lang.Boolean  修改成功->true   失败->false
     */
    @RequestMapping(value = "/portion/attribute/modify/{userId}", method = RequestMethod.POST)
    @ApiOperation(value = "修改用户扩展字段部分数据")
    Boolean portionAttributeModify(@PathVariable(value = "userId") @NotNull(message = "用户id不能为null") String userId,
                                   @RequestBody @NotNull(message = "修改数据不能为null") MiniAccountExtendsUpdateDto miniAccountExtendsUpdateDto);

    /**
     * 获取用户信息接口
     *
     * @param userId 用户id
     * @param infos  [1,2,3,4]  1,基本信息,2,扩展信息,3-地址信息,4-授权信息 5-用户所属团长信息  需要哪些发哪些 list
     * @return com.medusa.gruul.account.api.model.AccountInfoDto
     */
    @RequestMapping(value = "/account/info/{userId}", method = RequestMethod.GET)
    @ApiOperation(value = "获取用户信息,注意判空")
    AccountInfoDto accountInfo(@PathVariable(value = "userId") @NotNull(message = "用户id不能为null") String userId,
                               @RequestParam(value = "infos") @NotNull(message = "获取数据数组不能为空") List<Integer> infos);

    /**
     * 批量获取指定用户id(用户店铺id)的用户基本信息
     *
     * @param shopUserId 用户id数组
     * @return com.medusa.gruul.account.api.model.AccountInfoDto
     */
    @RequestMapping(value = "/accounts/info", method = RequestMethod.GET)
    @ApiOperation(value = "批量获取指定用户id(用户店铺id)的用户基本信息")
    List<MiniAccountExtDto> accountsInfoList(@RequestParam(value = "shopUserId") @NotNull(message = "用户id不能为null") List<String> shopUserId);



}
