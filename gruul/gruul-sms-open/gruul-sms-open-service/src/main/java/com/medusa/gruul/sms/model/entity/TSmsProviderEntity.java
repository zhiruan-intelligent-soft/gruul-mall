package com.medusa.gruul.sms.model.entity;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * t_sms_provider表
 * 
 * @author system
 * 
 */
@Data
public class TSmsProviderEntity implements Serializable{

	private Long id;
	private Long userId;
	private String smsProviderName;
	private String smsProviderAppId;
	private String smsProviderAppSecret;
	private Integer smsProviderAvailableCount;
	private Integer smsProviderUsedCount;
	private Integer smsProviderTotalCount;
	private Long smsProviderStatus;
	private Long isDeleted;
	private Date createTime;
	private Date updateTime;

	public TSmsProviderEntity() {
		super();
		clear();
	}

	public void clear() {
		id = null;
		userId = null;
		smsProviderName = null;
		smsProviderAppId = null;
		smsProviderAppSecret = null;
		smsProviderAvailableCount = null;
		smsProviderUsedCount = null;
		smsProviderTotalCount = null;
		smsProviderStatus = null;
		isDeleted = null;
		createTime = null;
		updateTime = null;
	}
}
