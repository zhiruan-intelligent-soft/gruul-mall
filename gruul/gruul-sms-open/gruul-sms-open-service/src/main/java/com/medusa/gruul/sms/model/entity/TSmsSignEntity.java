package com.medusa.gruul.sms.model.entity;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * t_sms_sign表
 * 
 * @author system
 * 
 */
@Data
public class TSmsSignEntity implements Serializable{

	private Long id;
	private Long userId;
	private Long smsProviderId;
	private String smsSign;
	private Long smsSignIsPass;
	private String smsSignRemark;
	private Long isDeleted;
	private Date createTime;
	private Date updateTime;
	private Integer smsSignType;
	private Integer signType;

	public TSmsSignEntity() {
		super();
		clear();
	}

	public void clear() {
		id = null;
		userId = null;
		smsProviderId = null;
		smsSign = null;
		smsSignIsPass = null;
		smsSignRemark = null;
		isDeleted = null;
		createTime = null;
		updateTime = null;
		smsSignType = null;
		signType = null;
	}



}
