package com.medusa.gruul.sms.task;

import cn.hutool.core.collection.CollectionUtil;
import com.medusa.gruul.sms.constant.SmsSmsStatus;
import com.medusa.gruul.sms.model.entity.TSmsOrderEntity;
import com.medusa.gruul.sms.service.SmsOrderService;
import com.medusa.gruul.sms.service.SmsSendService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Copyright(C) 2019-12-29 22:38 美杜莎 Inc.ALL Rights Reserved.
 *
 * @version V1.0
 * @description: 美杜莎 短信发送job
 * @author: wangpeng
 * @date: 2019-12-29 22:38
 **/
@Slf4j
@Component
public class SendSmsTask  {

    private final SmsOrderService smsOrderService;
    private final SmsSendService smsSendService;

    public SendSmsTask(SmsOrderService smsOrderService, SmsSendService smsSendService) {
        this.smsOrderService = smsOrderService;
        this.smsSendService = smsSendService;
    }


    @Scheduled(fixedRate = 1000 * 5)
    public  String execute(){
        System.out.println("-----------短信发送任务开启-----------");
        log.info("-----------短信发送任务开启-----------");
        try {
            List<TSmsOrderEntity> data = getData();
            if (!CollectionUtil.isEmpty(data)) {
                data.forEach(smsSendService::sendSms);
            } else {
                log.info("-----------无待发送短信订单，跳过本次发送-----------");
            }

        } catch (Exception e) {

            log.error("短信发送异常", e);
        }

        log.info("-----------短信发送任务结束-----------");
        return "";
    }

    private List<TSmsOrderEntity> getData() {

        return smsOrderService.doListWaitSendOrder(SmsSmsStatus.WAIT_SEND);
    }


}
