package com.medusa.gruul.sms.model.entity;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * t_sms_order表
 * 
 * @author system
 * 
 */
@TableName("t_sms_order")
@Data
public class TSmsOrderEntity implements Serializable{

	private Long id;
	private Long userId;
	private Long smsSendTime;
	private Long smsSendType;
	private String smsSendContent;
	private String smsSendZone;
	private String smsSendParam;
	private String smsSendMobiles;
	private Integer smsSendCount;
	private Integer smsSendSuccessCount;
	private Integer smsSendFailCount;
	private Integer smsSendStatus;
	private Long providerId;
	private Long smsType;
	private Long isDeleted;
	private Date createTime;
	private Date updateTime;
	private Long templateId;
	private Long signId;

	public TSmsOrderEntity() {
		super();
		clear();
	}

	public void clear() {
		id = null;
		userId = null;
		smsSendTime = null;
		smsSendType = null;
		smsSendContent = null;
		smsSendZone = null;
		smsSendParam = null;
		smsSendMobiles = null;
		smsSendCount = null;
		smsSendSuccessCount = null;
		smsSendFailCount = null;
		smsSendStatus = null;
		providerId = null;
		smsType = null;
		isDeleted = null;
		createTime = null;
		updateTime = null;
		templateId = null;
		signId = null;
	}



}
