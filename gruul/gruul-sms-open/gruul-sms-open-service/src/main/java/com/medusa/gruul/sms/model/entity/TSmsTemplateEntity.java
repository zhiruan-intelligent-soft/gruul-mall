package com.medusa.gruul.sms.model.entity;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;


/**
 * t_sms_template表
 * 
 * @author system
 * 
 */
@Data
public class TSmsTemplateEntity implements Serializable{

	private Long id;
	private Long userId;
	private Long smsProviderId;
	private String smsTemplateContent;
	private String smsTemplateCode;
	private Long smsTemplateIsPass;
	private String smsRemark;
	private Long isDeleted;
	private Date createTime;
	private Date updateTime;
	private Integer templateType;
	private Integer smsTemplateType;
	private String smsTemplateName;

	public TSmsTemplateEntity() {
		super();
		clear();
	}

	public void clear() {
		id = null;
		userId = null;
		smsProviderId = null;
		smsTemplateContent = null;
		smsTemplateCode = null;
		smsTemplateIsPass = null;
		smsRemark = null;
		isDeleted = null;
		createTime = null;
		updateTime = null;
		templateType = null;
		smsTemplateType = null;
		smsTemplateName = null;
	}



}
