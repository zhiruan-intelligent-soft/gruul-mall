 <h4></h4>
   <a href="https://www.73app.cn/" style="color: #095eab; text-decoration: none; font-weight: 600" target="_blank">
 <img  src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gruul/20240307/0cf0ebb6fff242529ce25b43fe6b6796.png">
</a>

<h2 align="center">
启山智软 Smart Shop Java 微服务电商中台
</h2>
    <h4></h4>
    <div style="text-align: center; margin-top: 20px" align="center">
        <a href="https://www.73app.cn/" style="color: #095eab; text-decoration: none; font-weight: 600" target="_blank"><b>官网</b></a> |
        <a href="https://pro.bgniao.cn/platform/#/" style="color: #095eab; text-decoration: none; font-weight: 600" target="_blank"><b>在线体验</b></a> |
        <a
            href="https://meizi2022.yuque.com/pm14ry/ooc7p0"
            style="color: #095eab; text-decoration: none; font-weight: 600"
            ><b>帮助中心</b></a>
        | <a href="https://meizi2022.yuque.com/pm14ry/fbw2ss/etn86rql8912pdpb#67RQ" style="color: #095eab; text-decoration: none; font-weight: 600" target="_blank"><b>功能清单</b></a>
   
</div>
<h4>✨ 介绍</h4>

 <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们的目标是打造更强大、更灵活、更易用的商城系统，最终成为 Java
            程序员的首选商城框架，让您的商城开发事半功倍！我们具有多元的商业模式满足您任何使用场景的需求，有<b>S2B2C供应链商城、B2B2C多商户商城、O2O外卖商城、B2C单商户商城、社区团购、B2B批发商城等众多商业模式</b>并含有<b>限时秒杀、直播、优惠券、满减、砍价、分销、套餐、拼团、消费返利、平台抽佣、储值、同城配送、到店自提、库存、代销</b>更有强大的<b>DIY装修功能</b>。
        </p>
<h4>✨ 采用JAVA JDK17搭配VUE3丶TS的商城系统</h4>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在此特邀您的加入！Smart Shop商城系统助您开启商业新篇章，快速构筑商城梦。</p> 
 <!--  <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;加入qq群,进行技术沟通和商业咨询：<b>QQ4群：476139879&nbsp;&nbsp;&nbsp;&nbsp;QQ5群：458320504</b> -->
</p>
       


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <thead>
    <tr>
   <th  width="300px" align="center" height="60">项目咨询 | 桐桐</th>
    </tr>
  </thead>
  <tbody>
    <tr>
   <td align="center">
      <img  src="./images/111.jpg">
      </td>
    </tr>
    <tr>
      <td align="center">VX号：18158554030</td>
    </tr>
   
  </tbody>
</table> 

<p>

<h4>🚀 优势</h4>
部分UI展示
      <img  src="./lQLPJw3xnWK0QQnNEnbNBH6w3ZcZiG-hjqIGXTgXg4GHAA_1150_4726.png">
      <img  src="./2024-06-19.png">

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <thead>
    <tr>
      <th  width="300px" align="center" height="60">采用技术</th>
      <th width="300px" align="center" height="60">版本</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="center">Java</td>
      <td align="center">JDK17</td>
    </tr>
    <tr>
      <td align="center">Spring Boot</td>
      <td align="center">2.7.7</td>
    </tr>
    <tr>
      <td align="center">Spring Cloud</td>
      <td align="center">2021.0.4</td>
    </tr>
    <tr>
      <td align="center">Spring Security OAuth2</td>
      <td align="center">2.5.0</td>
    </tr>
    <tr>
      <td align="center">Nacos</td>
      <td align="center">2.1.0</td>
    </tr>
    <tr>
      <td align="center">Spring Cloud Gateway</td>
      <td align="center">3.1.4</td>
    </tr>
    <tr>
      <td align="center">Sentinel</td>
      <td align="center">1.8.5</td>
    </tr>
    <tr>
      <td align="center">Dubbo</td>
      <td align="center">3.2.4</td>
    </tr>
    <tr>
      <td align="center">Fastjson2</td>
      <td align="center">2.0.34</td>
    </tr>
    <tr>
      <td align="center">Redis</td>
      <td align="center">6.2.6</td>
    </tr>
    <tr>
      <td align="center">Redisson</td>
      <td align="center">3.16.7</td>
    </tr>
    <tr>
      <td align="center">RabbitMQ</td>
      <td align="center">3.8.3</td>
    </tr>
    <tr>
      <td align="center">ElasticSearch</td>
      <td align="center">7.14.0</td>
    </tr>
    <tr>
      <td align="center">XXL-Job</td>
      <td align="center">2.3.1</td>
    </tr>
    <tr>
      <td align="center">Mysql</td>
      <td align="center">8.0.24</td>
    </tr>
    <tr>
      <td align="center">Mybatis-Plus</td>
      <td align="center">3.5.3.1</td>
    </tr>
    <tr>
      <td align="center">Smart-DOC</td>
      <td align="center">2.5.3</td>
    </tr>
    <tr>
      <td align="center">Hutool</td>
      <td align="center">5.8.16</td>
    </tr>
    <tr>
      <td align="center">Vavr</td>
      <td align="center">0.10.4</td>
    </tr>
    <tr>
      <td align="center">Logback</td>
      <td align="center">1.2.11</td>
    </tr>
    <tr>
      <td align="center">WxJava</td>
      <td align="center">4.5.0</td>
    </tr>
    <tr>
      <td align="center">TypeScript</td>
      <td align="center">4.6.4</td>
    </tr>
    <tr>
      <td align="center">Vue</td>
      <td align="center">3.2.45</td>
    </tr>
    <tr>
      <td align="center">Element Plus</td>
      <td align="center">2.1.1</td>
    </tr>
    <tr>
      <td align="center">Pinia</td>
      <td align="center">2.0.22</td>
    </tr>
    <tr>
      <td align="center">Vite</td>
      <td align="center">3.2.2</td>
    </tr>
    <tr>
      <td align="center">uni-app</td>
      <td align="center">***</td>
    </tr>
    <tr>
      <td align="center">vk-uview-ui</td>
      <td align="center">1.5.0</td>
    </tr>

  </tbody>
</table>
<p align="center" >
    <img  src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gruul/20230919/cc6539185e704d67b8b643013231fa63.jpeg"
msg
">
</p>

<h5>🚀 部分代码展示</h4>
<p>
    <img  src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gruul/20230802/efbd21f30f3844c38fa65b166b525c2e.png">
    <img  src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gruul/20230802/af2882bb2bc048c39110e08f9009a920.png">
    <img  src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/s2b2c_2.0.8/2024/7/2666a35ff4e4b0dd23dbbd3a10.png">

</p>

<h4>🚀 技术架构</h4>
   <img    src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gruul/20230812/dd22f7150ad74ad499cd4d6ef130b46e.png"><br>


<h4>✨ 说明</h4>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;此仓库为启山智软推出新零售B2C商城开源项目，开源项目不包含营销应用，开源不易丶记得点点Star⭐，如咨询其他电商系统或营销应用相关源码请联系下方客服。
</p>

<h4>🚀 荣誉资质</h4>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <thead>
    <tr>
      <th  width="300px" align="center" height="60"><img  src="https://images.gitee.com/uploads/images/2021/0907/162352_817a9646_8533008.png"></th>
      <th  width="300px" align="center" height="60"><img  src="https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gruul/20231207/4c6f99a5df974972bffa9bb98f8d973f.jpeg""></th>
  <th  width="300px" align="center" height="60">
 <img  src="https://images.gitee.com/uploads/images/2021/0814/104233_f71a9b70_8533008.png">
  </th>
    </tr>
  </thead>
  </table>
<h4>🚀 参与贡献！-Participate in contributing</h4>
<h4></h4>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码农的心声，唯有码农能懂。Smart Shop诞生的背后是八年间技术沉淀与团队几十余人进行反复推敲与打磨，是经过对市场调研的不断推翻和重构。</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在此特别鸣谢项目中付出大量心血的团队成员：</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;项目发起人：山鸽</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;产品经理：美子、美少女、沈文广</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;系统总架构师：范范、张志保</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;前端开发：斯巴达、罗天师 yyds、龙哥、波波、pengyong、SongBingYan、Liufly、木三</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;后端开发：白沙群岛、老头、杰哥、阴川蝴蝶君、阿帕奇、机器猫、张志保、Xiaoq、jipeng、Daniel、王康</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;测试：聂小倩、铁柱、十又、黄适、泓宇、谭盛</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运维：不吃辣的子奇</p>




<p align="center"><b>欢迎来到我们的开源项目！创新、协作、高质量的代码。您的Star🌟，是我们前进的动力！ 💪✨🏆</b></p>